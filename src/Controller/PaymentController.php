<?php
namespace App\Controller;

use App\Entity\Payment;
use App\Repository\PaymentRepository;
use AppBundle\Event\SmsEvent;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Client;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class PaymentController
 * @package App\Controller
 */

class PaymentController extends AbstractController
{

    /**
     * @param Request $request
     * @param ManagerRegistry $doctrine
     * @param EventDispatcherInterface $dispatcher
     * @return JsonResponse
     * @throws \Exception
     * @Route("/payment", name="add_payment", methods={"POST"})
     */
    public function addPayment(Request $request, ManagerRegistry $doctrine, EventDispatcherInterface $dispatcher): JsonResponse
    {
        try {
            $entityManager = $doctrine->getManager();
            $request = $this->transformJsonBody($request);

            if (!$request || !$request->get('amount') || !$request->get('payed_at') ||
                !$request->get('name') || !$request->get('surname')){
                throw new \Exception();
            }

            $payment = new Payment();

            $amount = $request->get('amount');
            $payed_at = new \DateTime($request->get('payed_at'));
            $client = new Client();

            $client->setName($request->get('name'));
            $client->setSurname($request->get('surname'));
            $client->addPayment($payment);

            $payment->setAmount($amount);
            $payment->setPayedAt($payed_at);
            $payment->setClient($client);


            $entityManager->persist($client);
            $entityManager->persist($payment);

            $entityManager->flush();

            $event = new SmsEvent('123456789');
            $dispatcher->dispatch($event, SmsEvent::NAME);

            $response = [
                'status' => 200,
                'success' => "Payment added successfully",
            ];

            return new JsonResponse($response, 200);


        }catch(\Exception $e){
            $response = [
                'status' => 422,
                'errors' => "Invalid data"
            ];
            return new JsonResponse($response, 422);
        }

    }

    /**
     * @param $id
     * @return Response
     * @param ManagerRegistry $doctrine
     * @throws \Exception
     * @Route("/payment/{id}", name="get_payment", methods={"GET"})
     */
    public function getPayment($id, ManagerRegistry $doctrine): Response
    {
        $payment = $this->getDoctrine()->getRepository(Payment::class)->findOneBy(['id' => $id]);

        if (!$payment) {
            $response = [
                'status' => 404,
                'errors' => "Payment not found"
            ];
            return new Response(
                $this->renderView('Payment/payment_template.html.twig', array(
                    'payment'    => "Payment not found"
                )),
                404
            );
        } else {
            $response = [
                'id' => $payment->getId(),
                'amount' => $payment->getAmount(),
                'payed_at' => $payment->getPayedAt(),
                'clientName' => $payment->getClient()->getName(),
                'clientSurname' => $payment->getClient()->getSurname()
            ];

            return new Response(
                $this->renderView('Payment/payment_template.html.twig', array(
                    'payment'    => $response
                )),
                200
            );
        }
    }

    protected function transformJsonBody(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        if ($data === null) {
            return $request;
        }

        $request->request->replace($data);

        return $request;
    }
}