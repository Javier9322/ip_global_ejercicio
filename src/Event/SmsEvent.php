<?php
namespace AppBundle\Event;

use App\Entity\Payment;
use Symfony\Contracts\EventDispatcher\Event;

class SmsEvent extends Event{

    /**
     * @var String $_num
     */
    private $_num;

    const NAME = "sms.event";

    public function __construct(String $num)
    {
        $this->_num = $num;
    }

    public function getNum()
    {
        return $this->_num;
    }

}