<?php

namespace App\EventSubscriber;

use AppBundle\Event\SmsEvent;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\TerminateEvent;


class SmsSubscriber implements EventSubscriberInterface
{

    /**
     * @var LoggerInterface $_logger
     */
    private $_logger;
    private $count=0;
    public function __construct(LoggerInterface $logger)
    {
        $this->_logger = $logger;
    }


    public function onKernelTerminate(TerminateEvent $event)
    {

    }

    public static function getSubscribedEvents()
    {
        return [
            SmsEvent::NAME => 'onPaymentDone',
        ];
    }

    public function onPaymentDone(SmsEvent $event)
    {

        if($event->getNum()){
            $clientNum = $event->getNum();
            $this->_logger->info('Mensaje enviado a '.$clientNum);
            $event->stopPropagation();
            return false;
        }else{
            $this->_logger->warning('El mensaje no se ha podido enviar');
            $event->stopPropagation();
            return false;
        }

    }
}
