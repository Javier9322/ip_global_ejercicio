<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PaymentTest extends WebTestCase
{
    public function testPostPaymentValido(): void
    {
        $payload = [
            'amount' => 15.10,
            'payed_at' => '2022-02-28',
            'name' => 'Javier',
            'surname' => 'Gonzalez'
        ];

        $client = static::createClient();
        $client->request('POST', '/payment', [], [], [], \json_encode($payload));

        $response = $client->getResponse();

        $this->assertJson($response->getContent());
        $this->assertSame(200, $response->getStatusCode());
    }

    public function testPostPaymentInvalido(): void
    {
        $payload = [
            'amount' => 15.10,
            'payed_at' => '2022-02-28',
            'name' => 'Javier'
        ];

        $client = static::createClient();
        $client->request('POST', '/payment', [], [], [], \json_encode($payload));

        $response = $client->getResponse();

        $this->assertJson($response->getContent());
        $this->assertNotEquals(200, $response->getStatusCode());
    }

    public function testGetPaymentId(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/payment/4');

        $response = $client->getResponse();
        $this->assertGreaterThan(
            0,
            $crawler->filter('html')->count()
        );
        $this->assertSame(200, $response->getStatusCode());

    }
}
